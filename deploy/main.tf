terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-igormishchuk"
    key            = "recipe-app.tfstate"
    region         = "eu-west-3"
    encrypt        = true
    dynamodb_table = "tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-3"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    MangedBy    = "Terraform"
  }
}

data "aws_region" "current" {}